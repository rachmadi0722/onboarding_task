﻿using System.ComponentModel.DataAnnotations;

namespace onboarding_task.Models
{
    public class Task
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Task Name")]
        public string TaskName { get; set; }

        [Required]
        [Display(Name = "Start Time")]
        public DateTime startTime { get; set; }

        [Required]
        [Display(Name = "End Time")]
        public DateTime endTime { get; set; }

        [Required]
        public string? userId { get; set; }
    }
}
