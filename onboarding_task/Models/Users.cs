﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace onboarding_task.Models
{
    public class Users : IdentityUser
    {
        public string UserName { get; set; }

        public string Email { get; set; }
    }
}
