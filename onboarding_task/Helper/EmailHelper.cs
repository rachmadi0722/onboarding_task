﻿using MailKit.Security;
using MimeKit;
using MimeKit.Text;
using MailKit.Net.Smtp;

namespace onboarding_task.Helper
{
    public class EmailHelper
    {
        public bool SendEmail(string userEmail, string confirmationLink)
        {

            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse("juston58@ethereal.email"));
            email.To.Add(MailboxAddress.Parse(userEmail));
            email.Subject = "Confirm your email";
            email.Body = new TextPart(TextFormat.Html) { Text = confirmationLink };

            using var smtp = new SmtpClient();
            smtp.Connect("smtp.ethereal.email", 587, SecureSocketOptions.StartTls);
            smtp.Authenticate("juston58@ethereal.email", "rZfEhwbKCEvAQfRTmu");
            var hasil = smtp.Send(email);
            smtp.Disconnect(true);
            return true;
        }


    }
}
