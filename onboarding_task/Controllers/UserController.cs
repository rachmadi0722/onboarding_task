﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using onboarding_task.Data;
using onboarding_task.Models;

namespace onboarding_task.Controllers
{
    public class UserController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private IPasswordHasher<IdentityUser> _passwordHasher;

        public UserController(UserManager<IdentityUser> userManager, IPasswordHasher<IdentityUser> passwordHash)
        {
            _userManager = userManager;
            _passwordHasher = passwordHash;
        }

        public IActionResult Index()
        {
            return View(_userManager.Users.ToList());
        }

        public async Task<IActionResult> Edit(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user != null)
                return View(user);
            else
                return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> Edit(string Id, string Email, string UserName, string PhoneNumber, string Password)
        {
            var user = await _userManager.FindByIdAsync(Id);
            if (user != null)
            {
                if (!string.IsNullOrEmpty(Email))
                    user.Email = Email;
                else
                    ModelState.AddModelError("", "Email cannot be empty");

                if (!string.IsNullOrEmpty(Password))
                    user.PasswordHash = _passwordHasher.HashPassword(user, Password);
                else
                    ModelState.AddModelError("", "Password cannot be empty");


                if (!string.IsNullOrEmpty(Email))
                {
                    user.UserName = UserName;
                    user.PhoneNumber = PhoneNumber;
                    IdentityResult result = await _userManager.UpdateAsync(user);
                    if (result.Succeeded)
                        return RedirectToAction("Index");
                    else
                        return View(user);
                }
            }
            else
                ModelState.AddModelError("", "User Not Found");
            return View(user);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user != null)
            {
                IdentityResult result = await _userManager.DeleteAsync(user);
                if (result.Succeeded)
                    return RedirectToAction("Index");
                else
                    View(user);
            }
            else
                ModelState.AddModelError("", "User Not Found");
            return View(user);
        }
    }
}
