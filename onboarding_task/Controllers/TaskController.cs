﻿using Microsoft.AspNetCore.Mvc;
using onboarding_task.Data;
using onboarding_task.Models;
using Task = onboarding_task.Models.Task;

namespace onboarding_task.Controllers
{
    public class TaskController : Controller
    {

        private readonly ApplicationDbContext _db;

        public TaskController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index(string? id)
        {
            IEnumerable<Task> obj = _db.Tasks.Where(m => m.userId ==  id);
            ViewBag.IdUser = id;
            return View(obj);
        }

        //GET
        public IActionResult Create(string? id)
        {
            Task Data = new Task
            {
                userId = (string)id
            };
            return View(Data);
        }

        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("TaskName,startTime,endTime,userId")] Task obj)
        {
            if (ModelState.IsValid)
            {
                _db.Tasks.Add(obj);
                _db.SaveChanges();
                return RedirectToAction("Index", new { id = obj.userId });
            }
            return View(obj);
        }

        //GET 
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var FromDb = _db.Tasks.Find(id);

            if (FromDb == null)
            {
                return NotFound();
            }

            return View(FromDb);
        }

        //PUT
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Task obj)
        {
            if (ModelState.IsValid)
            {
                _db.Tasks.Update(obj);
                _db.SaveChanges();
                return RedirectToAction("Index", new { id = obj.userId });
            }
            return View(obj);
        }

        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var itemFromDb = _db.Tasks.Find(id);

            if (itemFromDb == null)
            {
                return NotFound();
            }

            return View(itemFromDb);
        }

        //Delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePOST(int? id)
        {
            var obj = _db.Tasks.Find(id);
            if (obj == null)
            {
                return NotFound();
            }

            _db.Tasks.Remove(obj);
            _db.SaveChanges();
            return RedirectToAction("Index", new { id = obj.userId });

        }


    }
}
